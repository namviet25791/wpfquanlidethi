﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using AbpWpfDemo.People.Dto;
using AutoMapper;
using Castle.Core.Logging;
using newPSG.PMS.Helper;

namespace AbpWpfDemo.People
{
    public class PersonAppService : AbpWpfDemoAppServiceBase, IPersonAppService
    {
        private readonly IApplicationServiceFactory _factory;
        private readonly IRepository<Person> _personRepository;

        public PersonAppService(IRepository<Person> personRepository, IApplicationServiceFactory factory)
        {
            _personRepository = personRepository;
            Logger = NullLogger.Instance;
            _factory= factory;
        }

        public async Task<GetPeopleOutput> GetAllPeopleAsync()
        {
            var sql = $@"select * from people";
            var items = await _factory.UnitOfWordDapper.QueryAsync<Person>(sql);
            return new GetPeopleOutput
                   {
                       People = Mapper.Map<List<PersonDto>>(items)
                   };
        }

        public async Task AddNewPerson(AddNewPersonInput input)
        {
            Logger.Debug("Adding a new person: " + input.Name);
            await _personRepository.InsertAsync(new Person { Name = input.Name });
        }
    }
}
